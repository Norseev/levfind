#!/bin/sh

#
# Copyright© 2021 S.A.Norseev. All rights reserved.
#
# Contacts: norseev@gmail.com
#
# License: see accompanying file ReadMe.txt
#

echo "lev_find version 0.0"

if [ -z "$1" ]; then
  echo "Ошибка: не указана строка для поиска"
  exit 1
fi

grep -r --color -iE --exclude-dir=".git" "$1"
